import java.util.ArrayList;
import java.lang.Math;

public class Boid {
    public double x;
    public double y;
    public double dx;
    public double dy;
    public ArrayList<Boid> history;

    public Boid(double xcenter, double ycenter, double xwidth, double ywidth) {
        x = xcenter + Math.random() * xwidth;
        y = ycenter + Math.random() * ywidth;
        dx = Math.random();
        dy = Math.random();
        history = new ArrayList<>();
    }

    /**
     * Distance from another Boid
     *
     * @param boid other boid
     * @return distance between the boids
     */
    public double distanceFrom(Boid boid) {
        return Math.sqrt(
                (this.x - boid.x) * (this.x - boid.x) +
                        (this.y - boid.y) * (this.y - boid.y)
                );
    }
}
