import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Representation of swarm of boids
 *
 * from https://github.com/beneater/boids/blob/master/boids.js
 */
public class Swarm {
    private ArrayList<Boid> boids;
    private double visualRange = Double.POSITIVE_INFINITY;
    private double minDistance = 1;

    public Swarm(int nboids, double xcenter, double ycenter, double xwidth, double ywidth) {
        boids = new ArrayList<>();
        for (int i=0; i<nboids; i++)
            boids.add(new Boid(xcenter, ycenter, xwidth, ywidth));
    }

    public void setVisualRange(double visualRange) {
        this.visualRange = visualRange;
    }

    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }

    /**
     * Naive and inneficient algorithm to find the n closest boids from
     * a reference boid
     *
     * @param boid reference void
     * @param n n closest neighboor
     * @return list of n closest boids
     */
    public ArrayList<Boid> nClosestBoids(Boid boid, int n) {
        List<Boid> sorted = boids.subList(0, boids.size());
        sorted.sort(Comparator.comparingDouble(boid::distanceFrom));
        return new ArrayList<>(sorted.subList(1, n));
    }

    /**
     * Find the center of mass of the other boids and adjust velocity slightly to
     * point towards the center of mass.
     *
     * @param boid reference boid
     */
    public void flyTowardsCernter(Boid boid) {
        double centeringFactor = 0.005; // adjust velocity by this %
        double centerx = 0.0;
        double centery = 0.0;
        int numNeighbors = 0;

        for (Boid otherBoid : boids) {
            if (boid.distanceFrom(otherBoid) < visualRange) {
                centerx += otherBoid.x;
                centery += otherBoid.y;
                numNeighbors += 1;
            }

            if (numNeighbors > 0) {
                centerx = centerx / numNeighbors;
                centery = centery / numNeighbors;

                boid.dx += (centerx - boid.x) * centeringFactor;
                boid.dy += (centery - boid.y) * centeringFactor;
            }
        }
    }

    public static void main(String[] args) {
        Swarm swarm = new Swarm(3, 0, 0, 1, 1);
        swarm.boids.forEach((s)->System.out.println(s.x + " " + s.y));
        System.out.println();
        System.out.println(swarm.boids.get(0).distanceFrom(swarm.boids.get(1)));
        System.out.println(swarm.boids.get(0).distanceFrom(swarm.boids.get(2)));
        System.out.println();
        ArrayList<Boid> lst = swarm.nClosestBoids(swarm.boids.get(0), 3);
        lst.forEach((s)->System.out.println(s.x + " " + s.y));
    }
}
